import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import glob
from PIL import Image
import cv2
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_percentage_error


def plot_forecast():
	folders = ["india/extended_split2V2_2021-08-14_2021-03-15_2021-06-10",
	           'india/extended_split2V2_2021-09-08_2021-03-15_2021-06-10_0_1',
	           'india/extended_split2V2_2021-10-07_2021-03-15_2021-06-10_1_9',
	           'india/extended_split2V2_2021-11-09_2021-03-15_2021-06-10',
	           'india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10']
	end_dates = ['2021-08-14',
	             '2021-09-08',
	             '2021-10-07',
	             '2021-11-09',
	             '2021-12-15']
	min_date = ''
	max_date = ''
	fig = plt.figure(figsize=(12, 5))
	ax1 = fig.add_subplot(121)
	ax2 = fig.add_subplot(122)

	for index, (end_date, folder) in enumerate(zip(end_dates, folders)):
		forecast_file = f'{folder}/india30.csv'
		df = pd.read_csv(forecast_file)
		dGs = df[df['series'] == 'dG']
		dDs = df[df['series'] == 'dD']
		if min_date == '' or min_date > dGs.columns[1]:
			min_date = dGs.columns[1]
		if max_date == '' or max_date < dGs.columns[-1]:
			max_date = dGs.columns[-1]
		dG_low = dGs.iloc[0, 1:]
		dG_high = dGs.iloc[1, 1:]
		dD_low = dDs.iloc[0, 1:]
		dD_high = dDs.iloc[1, 1:]
		# print(dG_low)
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_low)
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_high)

		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_low)
		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_high)

	x1_low, x1_high = ax1.get_xlim()
	y1_low, y1_high = ax1.get_ylim()

	x2_low, x2_high = ax2.get_xlim()
	y2_low, y2_high = ax2.get_ylim()
	print(y1_low, y1_high)
	plt.close(fig)
	# fig.autofmt_xdate()
	# plt.show()

	ConfirmFile = 'india/indian_cases_confirmed_cases.csv'
	DeathFile = 'india/indian_cases_confirmed_deaths.csv'
	confirm_data_df = pd.read_csv(ConfirmFile)
	death_data_df = pd.read_csv(DeathFile)
	confirm_data_df = confirm_data_df.iloc[:, 1:]
	death_data_df = death_data_df.iloc[:, 1:]
	confirmed = []
	death = []
	for i in confirm_data_df.columns:
		confirmed.append(sum(confirm_data_df[i]))
		death.append(sum(death_data_df[i]))
	confirmed_df = pd.DataFrame([confirmed], columns=confirm_data_df.columns)
	death_df = pd.DataFrame([death], columns=death_data_df.columns)

	d_confirmed = [0]
	d_death = [0]
	for i in range(1, len(confirmed_df.columns)):
		d_confirmed.append(confirmed_df.iloc[0, i] - confirmed_df.iloc[0, i - 1])
		d_death.append(death_df.iloc[0, i] - death_df.iloc[0, i - 1])
	d_confirmed_df = pd.DataFrame([d_confirmed], columns=confirm_data_df.columns)
	d_death_df = pd.DataFrame([d_death], columns=death_data_df.columns)

	for index, (end_date, folder) in enumerate(zip(end_dates, folders)):
		fig = plt.figure(figsize=(12, 5))
		ax1 = fig.add_subplot(121)
		ax2 = fig.add_subplot(122)
		ax1.set_title('Daily Cases (Thousand)')
		ax2.set_title('Daily Deaths (Thousand)')
		forecast_file = f'{folder}/india30.csv'
		df = pd.read_csv(forecast_file)
		dGs = df[df['series'] == 'dG']
		dDs = df[df['series'] == 'dD']
		dG_low = list(dGs.iloc[1, 1:])
		dG_high = list(dGs.iloc[0, 1:])
		dD_low = list(dDs.iloc[1, 1:])
		dD_high = list(dDs.iloc[0, 1:])
		dG_low = [i / 1000 for i in dG_low]
		dG_high = [i / 1000 for i in dG_high]
		dD_low = [i / 1000 for i in dD_low]
		dD_high = [i / 1000 for i in dD_high]
		# print(end_date)
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_low, color='orange')
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_high, color='red')

		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_low, color='orange')
		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_high, color='red')

		ax1.fill_between([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_high, dG_low,
		                 where=[dG_high[i] > dG_low[i] for i in range(len(dG_high))], alpha=0.3, color='orange')
		ax2.fill_between([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_high, dD_low,
		                 where=[dD_high[i] > dD_low[i] for i in range(len(dD_high))], alpha=0.3, color='orange')

		dates = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in
		         list(confirm_data_df.iloc[0].loc[dGs.columns[1]:end_date].index)]
		d_confirmed = [i / 1000 for i in d_confirmed_df.iloc[0].loc[dGs.columns[1]:end_date]]
		d_death = [i / 1000 for i in d_death_df.iloc[0].loc[dDs.columns[1]:end_date]]
		ax1.scatter(dates, d_confirmed, s=1, zorder=1, color='grey')
		ax2.scatter(dates, d_death, s=1, zorder=1, color='grey')
		# ax1.scatter(dates, d_confirmed_df.iloc[0].loc[dGs.columns[1]:end_date], s=1, zorder=1, color='grey')
		# ax2.scatter(dates, d_death_df.iloc[0].loc[dDs.columns[1]:end_date], s=1, zorder=1, color='grey')

		ax1.set_xlim(x1_low, x1_high)
		ax1.set_ylim(y1_low / 1000, y1_high / 1000)
		ax2.set_xlim(x2_low, x2_high)
		ax2.set_ylim(y2_low / 1000, y2_high / 1000)
		fig.autofmt_xdate()
		# fig.savefig(f'india/comparison/{index}.png')
		plt.show()
		plt.close(fig)
	return


# add Omicron in December
def plot_forecast_V2():
	titles = ['August Forecast', 'September Forecast', 'October Forecast', 'November Forecast', 'December Forecast',
	          'December Forecast (0.15 Lockdown)', 'December Forecast (0.2 Lockdown)',
	          'December Forecast (0.25 Lockdown)']
	csv_files = ["india/extended_split2V2_2021-08-14_2021-03-15_2021-06-10/india30.csv",
	             'india/extended_split2V2_2021-09-08_2021-03-15_2021-06-10_0_1/india30.csv',
	             'india/extended_split2V2_2021-10-07_2021-03-15_2021-06-10_1_9/india30.csv',
	             'india/extended_split2V2_2021-11-09_2021-03-15_2021-06-10/india30.csv',
	             'india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10/india30_0_0.5.csv',
	             'india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10/india30_0.15_0.5.csv',
	             'india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10/india30_0.2_0.5.csv',
	             'india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10/india30_0.25_0.5.csv']
	end_dates = ['2021-08-14',
	             '2021-09-08',
	             '2021-10-07',
	             '2021-11-09',
	             '2021-12-15',
	             '2021-12-15',
	             '2021-12-15',
	             '2021-12-15']
	figure_names = ['india/comparison/India_30Aug1.png',
	                'india/comparison/India_30Sept1.png',
	                'india/comparison/India_30Oct1.png',
	                'india/comparison/India_30Nov1.png',
	                'india/comparison/India_30Dec1.png',
	                'india/comparison/lockdown/0.15.png',
	                'india/comparison/lockdown/0.2.png',
	                'india/comparison/lockdown/0.25.png']
	texts = [
		'Projection for India starting from August 2021.\n',

		'Projection for India starting from September 2021.\n',

		'The optimistic scenario shows improvements. Projection for India\n'
		'starting from October 2021.',

		'The optimistic scenario shows further improvements. Projection for\n'
		'India starting from November 2021.',

		'The Omicron impact is clearly illustrated. Projection for India starting\n'
		'from December 2021.']
	min_date = ''
	max_date = ''
	fig_width, fig_height = 14, 3.2
	fig = plt.figure(figsize=(fig_width, fig_height))
	ax1 = fig.add_subplot(121)
	ax2 = fig.add_subplot(122)

	for index, (end_date, csv_file) in enumerate(zip(end_dates, csv_files)):
		df = pd.read_csv(csv_file)
		dGs = df[df['series'] == 'dG']
		dDs = df[df['series'] == 'dD']
		if min_date == '' or min_date > dGs.columns[1]:
			min_date = dGs.columns[1]
		if max_date == '' or max_date < dGs.columns[-1]:
			max_date = dGs.columns[-1]
		dG_low = dGs.iloc[1, 1:]
		dG_high = dGs.iloc[0, 1:]
		dD_low = dDs.iloc[1, 1:]
		dD_high = dDs.iloc[0, 1:]

		if index == 4:
			dG_Oms = df[df['series'] == 'dG_Om']
			dG_Om_low = dG_Oms.iloc[1, 1:]
			dG_Om_high = dG_Oms.iloc[0, 1:]
			dG_low = dG_low.add(dG_Om_low)
			dG_high = dG_high.add(dG_Om_high)

		# print(dG_low)
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_low)
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_high)

		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_low)
		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_high)

	# x1_low, x1_high = ax1.get_xlim()
	x1_low = datetime.datetime.strptime(dGs.columns[1], '%Y-%m-%d')
	x1_high = datetime.datetime.strptime(dGs.columns[-1], '%Y-%m-%d')
	y1_low, y1_high = ax1.get_ylim()

	# x2_low, x2_high = ax2.get_xlim()
	x2_low = datetime.datetime.strptime(dDs.columns[1], '%Y-%m-%d')
	x2_high = datetime.datetime.strptime(dDs.columns[-1], '%Y-%m-%d')
	y2_low, y2_high = ax2.get_ylim()
	# print(y1_low, y1_high)
	plt.close(fig)
	# fig.autofmt_xdate()
	# plt.show()

	ConfirmFile = 'india/indian_cases_confirmed_cases.csv'
	DeathFile = 'india/indian_cases_confirmed_deaths.csv'
	confirm_data_df = pd.read_csv(ConfirmFile)
	death_data_df = pd.read_csv(DeathFile)
	confirm_data_df = confirm_data_df.iloc[:, 1:]
	death_data_df = death_data_df.iloc[:, 1:]
	confirmed = []
	death = []
	for i in confirm_data_df.columns:
		confirmed.append(sum(confirm_data_df[i]))
		death.append(sum(death_data_df[i]))
	confirmed_df = pd.DataFrame([confirmed], columns=confirm_data_df.columns)
	death_df = pd.DataFrame([death], columns=death_data_df.columns)

	d_confirmed = [0]
	d_death = [0]
	for i in range(1, len(confirmed_df.columns)):
		d_confirmed.append(confirmed_df.iloc[0, i] - confirmed_df.iloc[0, i - 1])
		d_death.append(death_df.iloc[0, i] - death_df.iloc[0, i - 1])
	d_confirmed_df = pd.DataFrame([d_confirmed], columns=confirm_data_df.columns)
	d_death_df = pd.DataFrame([d_death], columns=death_data_df.columns)

	for index, (end_date, csv_file, title, figure_name, text) in enumerate(
			zip(end_dates, csv_files, titles, figure_names, texts)):
		fig = plt.figure(figsize=(fig_width, fig_height))
		fig.suptitle(title)
		ax1 = fig.add_subplot(121)
		ax2 = fig.add_subplot(122)
		ax1.set_title('Daily Cases (Thousand)')
		ax2.set_title('Daily Deaths (Thousand)')
		df = pd.read_csv(csv_file)
		dGs = df[df['series'] == 'dG']
		dDs = df[df['series'] == 'dD']
		dG_low = dGs.iloc[1, 1:]
		dG_high = dGs.iloc[0, 1:]
		dD_low = dDs.iloc[1, 1:]
		dD_high = dDs.iloc[0, 1:]

		if index >= 4:
			dG_Oms = df[df['series'] == 'dG_Om']
			dG_Om_low = dG_Oms.iloc[1, 1:]
			dG_Om_high = dG_Oms.iloc[0, 1:]
			dG_low = dG_low.add(dG_Om_low)
			dG_high = dG_high.add(dG_Om_high)

		dG_low = [i / 1000 for i in dG_low]
		dG_high = [i / 1000 for i in dG_high]
		dD_low = [i / 1000 for i in dD_low]
		dD_high = [i / 1000 for i in dD_high]
		# print(end_date)
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_low, color='orange',
		         label='Optimistic')
		ax1.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_high, color='red',
		         label='Pessimistic')

		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_low, color='orange')
		ax2.plot([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_high, color='red')

		ax1.fill_between([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dGs.columns[1:]], dG_high, dG_low,
		                 where=[dG_high[i] > dG_low[i] for i in range(len(dG_high))], alpha=0.3, color='orange')
		ax2.fill_between([datetime.datetime.strptime(d, '%Y-%m-%d') for d in dDs.columns[1:]], dD_high, dD_low,
		                 where=[dD_high[i] > dD_low[i] for i in range(len(dD_high))], alpha=0.3, color='orange')

		if index >= 5:
			dates = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in
			         list(confirm_data_df.iloc[0].loc[dGs.columns[1]:].index)]
			d_confirmed = [i / 1000 for i in d_confirmed_df.iloc[0].loc[dGs.columns[1]:]]
			d_death = [i / 1000 for i in d_death_df.iloc[0].loc[dDs.columns[1]:]]
		else:
			dates = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in
			         list(confirm_data_df.iloc[0].loc[dGs.columns[1]:end_date].index)]
			d_confirmed = [i / 1000 for i in d_confirmed_df.iloc[0].loc[dGs.columns[1]:end_date]]
			d_death = [i / 1000 for i in d_death_df.iloc[0].loc[dDs.columns[1]:end_date]]
		ax1.scatter(dates, d_confirmed, s=1, zorder=1, color='grey', label='Reported')
		ax2.scatter(dates, d_death, s=1, zorder=1, color='grey')
		ax1.axvspan(dates[0], datetime.datetime.strptime(end_date, '%Y-%m-%d'), color='blue', alpha=0.1)
		ax2.axvspan(dates[0], datetime.datetime.strptime(end_date, '%Y-%m-%d'), color='blue', alpha=0.1)
		# ax1.axvline(datetime.datetime.strptime(end_date, '%Y-%m-%d'), c='grey', linestyle=':', label='Data end')
		# ax2.axvline(datetime.datetime.strptime(end_date, '%Y-%m-%d'), c='grey', linestyle=':')
		# ax1.scatter(dates, d_confirmed_df.iloc[0].loc[dGs.columns[1]:end_date], s=1, zorder=1, color='grey')
		# ax2.scatter(dates, d_death_df.iloc[0].loc[dDs.columns[1]:end_date], s=1, zorder=1, color='grey')

		ax1.set_xlim(x1_low, x1_high)
		ax1.set_ylim(y1_low / 1000, y1_high / 1000)
		ax2.set_xlim(x2_low, x2_high)
		ax2.set_ylim(y2_low / 1000, y2_high / 1000)

		ax1.legend(loc='upper left')

		# ax1.set_facecolor('lightcyan')
		# ax2.set_facecolor('lightcyan')
		# fig.set_facecolor('azure')
		fig.autofmt_xdate()
		plt.subplots_adjust(bottom=0.3)
		# fig.text(0.1, 0.05, text, fontsize=16)
		fig.savefig(figure_name, bbox_inches="tight")
		# plt.show()
		plt.close(fig)

		# print the peak daily numbers
		print(figure_name)
		print(max(dG_high[dates.index(datetime.datetime.strptime(end_date, '%Y-%m-%d')):]))
		print(max(dG_low[dates.index(datetime.datetime.strptime(end_date, '%Y-%m-%d')):]))
	return


def make_gif():
	fp_in = 'india/comparison/Inida_30*.png'
	fp_out = 'india/comparison/forecast.gif'
	imgs = (Image.open(f) for f in sorted(glob.glob(fp_in)))
	img = next(imgs)
	img.save(fp=fp_out, format='GIF', append_images=imgs,
	         save_all=True, duration=1000, loop=0)
	return


def forecast_accuracy():
	ConfirmFile = 'india/indian_cases_confirmed_cases.csv'
	DeathFile = 'india/indian_cases_confirmed_deaths.csv'
	confirm_data_df = pd.read_csv(ConfirmFile)
	death_data_df = pd.read_csv(DeathFile)
	confirm_data_df = confirm_data_df.iloc[:, 1:]
	death_data_df = death_data_df.iloc[:, 1:]
	confirmed = []
	death = []
	for i in confirm_data_df.columns:
		confirmed.append(sum(confirm_data_df[i]))
		death.append(sum(death_data_df[i]))
	confirmed_df = pd.DataFrame([confirmed], columns=confirm_data_df.columns)
	death_df = pd.DataFrame([death], columns=death_data_df.columns)

	d_confirmed = [0]
	d_death = [0]
	for i in range(1, len(confirmed_df.columns)):
		d_confirmed.append(confirmed_df.iloc[0, i] - confirmed_df.iloc[0, i - 1])
		d_death.append(death_df.iloc[0, i] - death_df.iloc[0, i - 1])
	d_confirmed_df = pd.DataFrame([d_confirmed], columns=confirm_data_df.columns)
	d_death_df = pd.DataFrame([d_death], columns=death_data_df.columns)
	csv_file = 'india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10/india30_0_0.5.csv'
	df = pd.read_csv(csv_file)
	dGs = df[df['series'] == 'dG']
	dDs = df[df['series'] == 'dD']
	Gs = df[df['series'] == 'G']
	Ds = df[df['series'] == 'D']
	G_low = Gs.iloc[1, 1:]
	G_high = Gs.iloc[0, 1:]
	D_low = Ds.iloc[1, 1:]
	D_high = Ds.iloc[0, 1:]
	dG_low = dGs.iloc[1, 1:]
	dG_high = dGs.iloc[0, 1:]
	dD_low = dDs.iloc[1, 1:]
	dD_high = dDs.iloc[0, 1:]
	dG_Oms = df[df['series'] == 'dG_Om']
	dG_Om_low = dG_Oms.iloc[1, 1:]
	dG_Om_high = dG_Oms.iloc[0, 1:]
	dG_low = dG_low.add(dG_Om_low)
	dG_high = dG_high.add(dG_Om_high)
	d_confirmed = d_confirmed_df.iloc[0].loc['2021-12-15':].iloc[:45]
	d_death = d_death_df.iloc[0].loc['2021-12-15':].iloc[:45]
	dates = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in d_confirmed.index]
	dG_low = dG_low['2021-12-15':].iloc[:45]
	dG_high = dG_high['2021-12-15':].iloc[:45]
	dD_low = dD_low['2021-12-15':].iloc[:45]
	dD_high = dD_high['2021-12-15':].iloc[:45]

	# dG_low = dG_low['2021-12-15':]
	# dG_high = dG_high['2021-12-15':]
	india_pop = 1210568112
	cols = ['metric', '15 days', '30days', '45 days']

	RMSEs_G = ['RMSE per capita(daily cases)']
	R2s_G = ['R2(daily cases)']
	MAPEs_G = ['MAPE(daily cases)']
	RMSEs_G.append(np.sqrt(mean_squared_error(d_confirmed.iloc[:15], dG_high.iloc[:15])) / india_pop)
	R2s_G.append(r2_score(d_confirmed.iloc[:15], dG_high.iloc[:15]))
	MAPEs_G.append(mean_absolute_percentage_error(d_confirmed.iloc[:15], dG_high.iloc[:15]))
	RMSEs_G.append(np.sqrt(mean_squared_error(d_confirmed.iloc[:30], dG_high.iloc[:30])) / india_pop)
	R2s_G.append(r2_score(d_confirmed.iloc[:30], dG_high.iloc[:30]))
	MAPEs_G.append(mean_absolute_percentage_error(d_confirmed.iloc[:30], dG_high.iloc[:30]))
	RMSEs_G.append(np.sqrt(mean_squared_error(d_confirmed.iloc[:45], dG_high.iloc[:45])) / india_pop)
	R2s_G.append(r2_score(d_confirmed.iloc[:45], dG_high.iloc[:45]))
	MAPEs_G.append(mean_absolute_percentage_error(d_confirmed.iloc[:45], dG_high.iloc[:45]))

	RMSEs_D = ['RMSE per capita(daily death)']
	R2s_D = ['R2(daily death)']
	MAPEs_D = ['MAPE(daily death)']
	RMSEs_D.append(np.sqrt(mean_squared_error(d_death.iloc[:15], dD_high.iloc[:15])) / india_pop)
	R2s_D.append(r2_score(d_death.iloc[:15], dD_high.iloc[:15]))
	MAPEs_D.append(mean_absolute_percentage_error(d_death.iloc[:15], dD_high.iloc[:15]))
	RMSEs_D.append(np.sqrt(mean_squared_error(d_death.iloc[:30], dD_high.iloc[:30])) / india_pop)
	R2s_D.append(r2_score(d_death.iloc[:30], dD_high.iloc[:30]))
	MAPEs_D.append(mean_absolute_percentage_error(d_death.iloc[:30], dD_high.iloc[:30]))
	RMSEs_D.append(np.sqrt(mean_squared_error(d_death.iloc[:45], dD_high.iloc[:45])) / india_pop)
	R2s_D.append(r2_score(d_death.iloc[:45], dD_high.iloc[:45]))
	MAPEs_D.append(mean_absolute_percentage_error(d_death.iloc[:45], dD_high.iloc[:45]))

	cum_confirmed = confirmed_df.iloc[0].loc['2021-12-15':]
	cum_death = death_df.iloc[0].loc['2021-12-15':]
	cum_confirmed = cum_confirmed.sub(confirmed_df.iloc[0].loc['2021-12-14'])
	cum_death = cum_death.sub(death_df.iloc[0].loc['2021-12-14'])
	cum_G_high = G_high['2021-12-15':]
	cum_G_high = cum_G_high.sub(G_high['2021-12-14'])
	cum_D_high = D_high['2021-12-15':]
	cum_D_high = cum_D_high.sub(D_high['2021-12-14'])

	RMSEs_cum_G = ['RMSE per capita(cumulative cases)']
	R2s_cum_G = ['R2(cumulative cases)']
	MAPEs_cum_G = ['MAPE(cumulative cases)']
	RMSEs_cum_G.append(np.sqrt(mean_squared_error(cum_confirmed.iloc[:15], cum_G_high.iloc[:15])) / india_pop)
	R2s_cum_G.append(r2_score(cum_confirmed.iloc[:15], cum_G_high.iloc[:15]))
	MAPEs_cum_G.append(mean_absolute_percentage_error(cum_confirmed.iloc[:15], cum_G_high.iloc[:15]))
	RMSEs_cum_G.append(np.sqrt(mean_squared_error(cum_confirmed.iloc[:30], cum_G_high.iloc[:30])) / india_pop)
	R2s_cum_G.append(r2_score(cum_confirmed.iloc[:30], cum_G_high.iloc[:30]))
	MAPEs_cum_G.append(mean_absolute_percentage_error(cum_confirmed.iloc[:30], cum_G_high.iloc[:30]))
	RMSEs_cum_G.append(np.sqrt(mean_squared_error(cum_confirmed.iloc[:45], cum_G_high.iloc[:45])) / india_pop)
	R2s_cum_G.append(r2_score(cum_confirmed.iloc[:45], cum_G_high.iloc[:45]))
	MAPEs_cum_G.append(mean_absolute_percentage_error(cum_confirmed.iloc[:45], cum_G_high.iloc[:45]))

	RMSEs_cum_D = ['RMSE per capita(cumulative deaths)']
	R2s_cum_D = ['R2(cumulative deaths)']
	MAPEs_cum_D = ['MAPE(cumulative deaths)']
	RMSEs_cum_D.append(np.sqrt(mean_squared_error(cum_death.iloc[:15], cum_D_high.iloc[:15])) / india_pop)
	R2s_cum_D.append(r2_score(cum_death.iloc[:15], cum_D_high.iloc[:15]))
	MAPEs_cum_D.append(mean_absolute_percentage_error(cum_death.iloc[:15], cum_D_high.iloc[:15]))
	RMSEs_cum_D.append(np.sqrt(mean_squared_error(cum_death.iloc[:30], cum_D_high.iloc[:30])) / india_pop)
	R2s_cum_D.append(r2_score(cum_death.iloc[:30], cum_D_high.iloc[:30]))
	MAPEs_cum_D.append(mean_absolute_percentage_error(cum_death.iloc[:30], cum_D_high.iloc[:30]))
	RMSEs_cum_D.append(np.sqrt(mean_squared_error(cum_death.iloc[:45], cum_D_high.iloc[:45])) / india_pop)
	R2s_cum_D.append(r2_score(cum_death.iloc[:45], cum_D_high.iloc[:45]))
	MAPEs_cum_D.append(mean_absolute_percentage_error(cum_death.iloc[:45], cum_D_high.iloc[:45]))

	out_df = pd.DataFrame(
		[RMSEs_G, R2s_G, MAPEs_G, RMSEs_D, R2s_D, MAPEs_D, RMSEs_cum_G, R2s_cum_G, MAPEs_cum_G, RMSEs_cum_D, R2s_cum_D,
		 MAPEs_cum_D], columns=cols)
	print(out_df)
	out_df.to_csv('india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10/accuracy.csv', index=False)

	return


def make_mp4():
	img_array = []
	for filename in glob.glob('india/comparison/India*.png'):
		img = cv2.imread(filename)
		height, width, layers = img.shape
		size = (width, height)
		img_array.append(img)
	fps = 24
	out = cv2.VideoWriter('india/comparison/forecast.mp4', cv2.VideoWriter_fourcc(*'mp4v'), fps, size)
	seconds = 2
	for i in range(len(img_array)):
		for _ in range(seconds * fps):
			out.write(img_array[i])
	out.release()
	return


def make_confirmed():
	df_confirmed = pd.read_csv('india/indian_cases_confirmed_cases.csv')
	confirmed = []
	cols = df_confirmed.columns[1:]
	for d in cols:
		confirmed.append(sum(df_confirmed[d]) / 1000000)
	cols = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in cols]
	df = pd.DataFrame([confirmed], columns=cols)
	return df


def scenario_comparison():
	confirmed_df = make_confirmed()
	path = 'india/extended_split2V2_2021-12-15_2021-03-15_2021-06-10'
	csv_files = [
		'india30_0_0.5.csv',
		'india60_0_0.5.csv',
		'india30_0_0.75.csv',
		'india60_0_0.75.csv'
	]
	labels = [
		'Fast Release,\n50% Efficacy',
		'Moderate Release,\n50% Efficacy',
		'Fast Release,\n75% Efficacy',
		'Moderate Release,\n75% Efficacy',
	]
	fig = plt.figure(figsize=(9, 4.5))
	ax1 = fig.add_subplot(121)
	ax2 = fig.add_subplot(122)
	for csv_file, label in zip(csv_files, labels):
		df = pd.read_csv(f'{path}/{csv_file}')
		G = list(df[df['series'] == 'G'].iloc[0, 300:df.columns.get_loc('2022-01-31')])
		dG = list(df[df['series'] == 'dG'].iloc[0, 300:df.columns.get_loc('2022-01-31')])
		dG_Om = list(df[df['series'] == 'dG_Om'].iloc[0, 300:df.columns.get_loc('2022-01-31')])
		G_total = []
		dates = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in
		         list(df.columns[300:df.columns.get_loc('2022-01-31')])]
		for i in range(len(G)):
			if len(G_total) == 0:
				G_total.append(G[i] + dG[i] + dG_Om[i])
			else:
				G_total.append(G_total[-1] + dG[i] + dG_Om[i])
		ax1.plot(dates, [G / 1000000 for G in G_total], label=label)

		G = list(df[df['series'] == 'G'].iloc[1, 300:df.columns.get_loc('2022-01-31')])
		dG = list(df[df['series'] == 'dG'].iloc[1, 300:df.columns.get_loc('2022-01-31')])
		dG_Om = list(df[df['series'] == 'dG_Om'].iloc[1, 300:df.columns.get_loc('2022-01-31')])
		G_total = []
		for i in range(len(G)):
			if len(G_total) == 0:
				G_total.append(G[i] + dG[i] + dG_Om[i])
			else:
				G_total.append(G_total[-1] + dG[i] + dG_Om[i])
		ax2.plot(dates, [G / 1000000 for G in G_total], label=label)
	confirmed_df = confirmed_df.iloc[:, confirmed_df.columns.get_loc(dates[0]):
	                                    confirmed_df.columns.get_loc(
		                                    datetime.datetime.strptime('2022-01-31', '%Y-%m-%d'))]
	ax1.scatter(confirmed_df.columns, confirmed_df, s=1, color='grey', label='Reported Cases')
	ax2.scatter(confirmed_df.columns, confirmed_df, s=1, color='grey', label='Reported Cases')
	ax1.axvspan(dates[0], datetime.datetime.strptime('2021-12-16', '%Y-%m-%d'), color='blue', alpha=0.1)
	ax2.axvspan(dates[0], datetime.datetime.strptime('2021-12-16', '%Y-%m-%d'), color='blue', alpha=0.1)
	ax1.set_xlim(dates[0], dates[-1])
	ax2.set_xlim(dates[0], dates[-1])
	y1, y2 = ax1.get_ylim()
	ax2.set_ylim(y1, y2)
	ax1.set_title('Pessimistic Scenario')
	ax2.set_title('Optimistic Scenario')
	ax1.set_ylabel('Cumulative Cases(Million)')
	ax2.set_ylabel('Cumulative Cases(Million)')
	# ax1.legend()
	ax2.legend(loc='lower left', bbox_to_anchor=(1, 0))
	fig.autofmt_xdate()
	fig.savefig(f'{path}/scenarios.png', bbox_inches="tight")
	# plt.show()
	return


def main():
	# plot_forecast()
	# plot_forecast_V2()
	# make_gif()
	# make_mp4()
	# forecast_accuracy()
	scenario_comparison()
	return


if __name__ == '__main__':
	main()
