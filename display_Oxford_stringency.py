import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import datetime as datetime
import numpy as np
from statsmodels.tsa.stattools import grangercausalitytests

oxford_file_path1 = 'Oxford_data/OxCGRT_IND_differentiated_withnotes_2021.csv'
oxford_file_path2 = 'Oxford_data/OxCGRT_IND_differentiated_withnotes_2022.csv'
ConfirmFile = 'IndiaUptoOct2022/indian_cases_confirmed_cases.csv'
DeathFile = 'IndiaUptoOct2022/indian_cases_confirmed_deaths.csv'

states = {}
states["Andhra Pradesh"] = "AP"
states["Arunachal Pradesh"] = "AR"
states["Assam"] = "AS"
states["Bihar"] = "BR"
states["Bihar****"] = "BR"
states["Chattisgarh"] = "CT"
states["Chhattisgarh"] = "CT"
states["Goa"] = "GA"
states["Gujarat"] = "GJ"
states["Haryana"] = "HR"
states["Haryana***"] = "HR"
states["Himachal Pradesh"] = "HP"
states["Himanchal Pradesh"] = "HP"
states["Jharkhand"] = "JH"
states["Jharkhand#"] = "JH"
states["Karnataka"] = "KA"
states["Karanataka"] = "KA"
states["Kerala"] = "KL"
states["Kerala***"] = "KL"
states["Madhya Pradesh"] = "MP"
states["Madhya Pradesh#"] = "MP"
states["Madhya Pradesh***"] = "MP"
states["Maharashtra"] = "MH"
states["Maharashtra***"] = "MH"
states["Manipur"] = "MN"
states["Meghalaya"] = "ML"
states["Mizoram"] = "MZ"
states["Nagaland"] = "NL"
states["Nagaland#"] = "NL"
states["Odisha"] = "OR"
states["Punjab"] = "PB"
states["Punjab***"] = "PB"
states["Rajasthan"] = "RJ"
states["Sikkim"] = "SK"
states["Tamil Nadu"] = "TN"
states["Telengana"] = "TG"
states["Telangana***"] = "TG"
states["Telengana***"] = "TG"
states["Tripura"] = "TR"
states["Uttarakhand"] = "UT"
states["Uttar Pradesh"] = "UP"
states["West Bengal"] = "WB"
states["Andaman and Nicobar Islands"] = "AN"
states["Chandigarh"] = "CH"
states["Chandigarh***"] = "CH"
states["Dadra and Nagar Haveli"] = "DN"
states["Dadar Nagar Haveli"] = "DN"
states["Daman and Diu"] = "DD"
states["Daman & Diu"] = "DD"
states["Delhi"] = "DL"
states["Jammu and Kashmir"] = "JK"
states["Ladakh"] = "LA"
states["Lakshadweep"] = "LD"
states["Pondicherry"] = "PY"
states["Puducherry"] = "PY"
states["Dadra and Nagar Haveli and Daman and Diu"] = "DN_DD"
states["Telangana"] = "TG"
states["India"] = "India"

statesI = ['kl', 'dl', 'tg', 'rj', 'hr', 'jk', 'ka', 'la', 'mh', 'pb', 'tn', 'up', 'ap', 'ut', 'or', 'wb', 'py', 'ch',
		   'ct', 'gj', 'hp', 'mp', 'br', 'mn', 'mz', 'ga', 'an', 'as', 'jh', 'ar', 'tr', 'nl', 'ml', 'sk', 'dn_dd',
		   'ld']

state_dict = {'up': 'Uttar Pradesh',
			  'mh': 'Maharastra',
			  'br': 'Bihar',
			  'wb': 'West Bengal',
			  'mp': 'Madhya Pradesh',
			  'tn': 'Tamil Nadu',
			  'rj': 'Rajesthan',
			  'ka': 'Karnataka',
			  'gj': 'Gujarat',
			  'ap': 'Andhra Pradesh',
			  'or': 'Odisha',
			  'tg': 'Telangana',
			  'kl': 'Kerala',
			  'jh': 'Jharkhand',
			  'as': 'Assam',
			  'pb': 'Punjab',
			  'ct': 'Chhattisgarh',
			  'hr': 'Haryana',
			  'dl': 'Delhi',
			  'jk': 'Jammu and Kashmir',
			  'ut': 'Uttarakhand',
			  'hp': 'Himachal Pradesh',
			  'tr': 'Tripura',
			  'ml': 'Meghalaya',
			  'mn': 'Manipur',
			  'nl': 'Nagaland',
			  'ga': 'Goa',
			  'ar': 'Arunachal Pradesh',
			  'py': 'Puducherry',
			  'mz': 'Mizoram',
			  'ch': 'Chandigarh',
			  'sk': 'Sikkim',
			  'dn_dd': 'Daman and Diu',
			  'an': 'Andaman and Nicobar',
			  'ld': 'Ladakh',
			  'la': 'Lakshdweep'
			  }


def read_india_cases(start_date, end_date):
	dft = pd.read_csv(ConfirmFile)
	start_index = dft.columns.get_loc(start_date)
	end_index = dft.columns.get_loc(end_date) + 2
	df = dft.iloc[:, start_index:end_index]
	days = list(dft.columns)
	days = days[days.index(start_date):days.index(end_date) + 1]
	confirmed = df.sum()
	# df = dft[:,loc[start_date:end_date]]
	# df2 = pd.read_csv(DeathFile)
	# lenS = len(days)
	# confirmed = [0] * lenS
	# death = [0] * lenS
	# for s in statesI :
	# 	confirmedState = df[df.iloc[:, 0] == s]
	# 	confirmedState = confirmedState.iloc[0]
	# 	confirmed = [confirmed[i] + confirmedState[i] for i in range(lenS) ]
	# 	# death = death +  df2[df2.iloc[:, 0] == s]
	# death = death.iloc[0].loc[start_date: end_date]
	return confirmed, days


def create_Rec_display():
	ret_series = []

	index = pd.date_range('2021-01-01', '2021-06-09')
	seriesR = pd.Series(0, index)
	ret_series.append(seriesR)

	index = pd.date_range('2021-06-10', '2021-07-26')
	seriesR = pd.Series(300000, index)
	ret_series.append(seriesR)

	index = pd.date_range('2021-07-27', '2021-08-19')
	seriesR = pd.Series(160000, index)
	ret_series.append(seriesR)

	index = pd.date_range('2021-08-20', '2021-09-12')
	seriesR = pd.Series(223000, index)
	ret_series.append(seriesR)

	index = pd.date_range('2021-09-13', '2021-10-12')
	seriesR = pd.Series(117000, index)
	ret_series.append(seriesR)

	index = pd.date_range('2021-10-13', '2021-11-16')
	seriesR = pd.Series(131000, index)
	ret_series.append(seriesR)

	index = pd.date_range('2021-11-17', '2021-12-10')
	seriesR = pd.Series(150000, index)
	ret_series.append(seriesR)

	index = pd.date_range('2021-12-11', '2022-01-08')
	seriesR = pd.Series(1300000, index)
	ret_series.append(seriesR)

	index = pd.date_range('2022-01-09', '2022-04-01')
	seriesR = pd.Series(890000, index)
	ret_series.append(seriesR)

	return ret_series


def read_oxford_data(confirmed, days):
	# url = "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/actions/IND/{YYYY-MM-DD}" + getCurrentDataTimeAsString()
	# r = requests.get(url, verify=False)
	# txt = ""
	# print(r)
	# if r.status_code == 200:
	# 	txt = r.text
	# 	return txt
	df1 = pd.read_csv(oxford_file_path1)
	df2 = pd.read_csv(oxford_file_path2)
	df = df1.append(df2, ignore_index=True)
	mask = df['Date'] > 20220401
	df = df[~mask]
	fig, ax = plt.subplots()
	colorS1 = 'lightgray'
	colorS2 = 'lightgray'
	confirmed7A = confirmed.rolling(7, min_periods=1).mean()
	dconfirmed = pd.Series(np.diff(confirmed))
	dconfirmed7A = pd.Series(np.diff(confirmed7A))
	first_legend = True
	for s in states:
		state_code = 'IND_' + states[s]
		if states[s] == 'India':
			state_code = 'IND'
			dfstate = df[df['Jurisdiction'] == 'NAT_TOTAL']
			Y = dfstate['StringencyIndex_NonVaccinated_ForDisplay']
			# Y1 = dfstate['ContainmentHealthIndex_WeightedAverage_ForDisplay']
			# Y1 = dfstate['ContainmentHealthIndex_NonVaccinated_ForDisplay']
			# Y1 = dfstate['GovernmentResponseIndex_WeightedAverage_ForDisplay']
			Y1 = dfstate['StringencyIndex_Vaccinated_ForDisplay']
			colorS1 = 'black'
			colorS2 = 'blue'
		else:
			dfstate = df[df['RegionCode'] == state_code]
			Y = dfstate['StringencyIndex_NonVaccinated_ForDisplay']
			# Y1 = dfstate['ContainmentHealthIndex_SimpleAverage_ForDisplay']
			# Y1 = dfstate['ContainmentHealthIndex_NonVaccinated_ForDisplay']
			# Y1 = dfstate['GovernmentResponseIndex_SimpleAverage_ForDisplay']
			Y1 = dfstate['StringencyIndex_Vaccinated_ForDisplay']

		X = dfstate['Date']
		X = [str(d) for d in X]
		X = [d[0:4] + '-' + d[4:6] + '-' + d[6:8] for d in X]
		Xdates = [datetime.datetime.strptime(d, '%Y-%m-%d') for d in X]

		# Plot the two columns
		if first_legend:
			first_legend = False
			ax.plot(Xdates, Y, color=colorS1, label='States')
		else:
			ax.plot(Xdates, Y, color=colorS1)
		ax.plot(Xdates, Y1, color=colorS2)
	# ax2= ax.twinx()
	# ax2.plot(Xdates, dconfirmed, color='red')
	# Set the title and labels of the axes
	# ax.set_title('Data Frame Plot')

	# Show the plot

	# print(s)
	# dconfirmed =
	# ax.plot(Xdates, dconfirmed, color='red')

	ax.plot(Xdates, Y, color=colorS1, label='Unvaccinated(India)')
	ax.plot(Xdates, Y1, color=colorS2, label='Vaccinated(India)')
	ax.set_ylim(0, 100)
	ax2 = ax.twinx()

	first_legend = True
	for i in range(1, len(dconfirmed7A) - 1):
		if dconfirmed7A[i - 1] < 50000 <= dconfirmed7A[i] <= dconfirmed7A[i + 1]:
			if first_legend:
				first_legend = False
				ax2.axvline(Xdates[i], color='red', alpha=1, linestyle=':', label='Rising 50K Cases(7-day AVG)')
			else:
				ax2.axvline(Xdates[i], color='red', alpha=1, linestyle=':')
			print(Xdates[i].strftime('%Y-%m-%d'), 'stringency:', Y.iloc[i])

	ax2.plot(Xdates, dconfirmed7A, color='red', label='Daily Cases(7-day AVG)')

	policyRec = create_Rec_display()
	# ax2.bar(Xdates, policyRec, color ='orange', alpha =0.5 )
	alphas = [0.6, 0.3]
	first_legend = True
	for i in range(len(policyRec)):
		policy = policyRec[i]
		# ax2.plot(policy, color='orange', alpha=alphas[i % 2])
		if first_legend:
			first_legend = False
			ax2.fill_between(policy.index, policy, edgecolor='orange', color='orange', alpha=alphas[i % 2],
							 label='AVG Forecast Peak(+30 days)')
		else:
			ax2.fill_between(policy.index, policy, edgecolor='orange', color='orange', alpha=alphas[i % 2])
	# plt.ticklabel_format(style='plain', axis='y')
	ax2.get_yaxis().set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
	ax2.set_ylim(0, ax2.get_ylim()[1])
	ax.set_xlim(Xdates[0], Xdates[-1])
	ax.legend(loc='lower left', bbox_to_anchor=(-0.12, 1.02))
	ax2.legend(loc='lower right', bbox_to_anchor=(1.2, 1.02))
	fig.autofmt_xdate()
	ax.set_ylabel('Policy Stringency Index')
	ax2.set_ylabel('Daily Cases')

	# plt.show()
	fig.savefig('plot/stringency.png', bbox_inches='tight')
	granger(Xdates, Y, Y1, policyRec)
	return


def granger(Xdates, Y, Y1, policyRec):
	policies = pd.Series()
	for policy in policyRec[1:]:
		policies = pd.concat([policies, policy])
	Y.index = Xdates
	Y1.index = Xdates
	Y = Y[Y.index >= policies.index[0]]
	Y1 = Y1[Y1.index >= policies.index[0]]
	Y.name = 'Y'
	Y1.name = 'Y1'
	policies.name = 'peak'
	df = pd.merge(Y, policies, right_index=True, left_index=True)
	print('******************************')
	print('Unvaccinated:')
	print('******************************')
	grangercausalitytests(df, maxlag=30)

	df = pd.merge(Y1, policies, right_index=True, left_index=True)
	print('******************************')
	print('Vaccinated:')
	print('******************************')
	grangercausalitytests(df, maxlag=30)
	return


def main():
	confirmed, days = read_india_cases('2021-01-01', '2022-04-01')
	read_oxford_data(confirmed, days)
	return


if __name__ == '__main__':
	main()
